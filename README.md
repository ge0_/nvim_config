Create the nvim config directory if non existent:

```
mkdir -p ~/.config/nvim
```

Copy files:

```
cd $REPO
cp * ~/.config/nvim -Rf
```

Enjoy.
